FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

MAINTAINER Authors name <support@cloudron.io>

RUN mkdir -p /app/code /run/ampache/sessions

WORKDIR /app/code

EXPOSE 8000

ENV AMPACHEVERSION=3.9.0

#proftpd
RUN apt-get update && apt-get install -y proftpd proftpd-mod-ldap
RUN rm -rf /var/log/proftpd && ln -s /run/proftpd /var/log/proftpd

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf

RUN a2disconf other-vhosts-access-log

# configure mod_php
RUN a2enmod rewrite
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_size 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.save_path /run/ampache/sessions && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_divisor 100

# ampache
RUN wget -O ampache.zip https://github.com/ampache/ampache/releases/download/${AMPACHEVERSION}/ampache-${AMPACHEVERSION}_all.zip && \
    unzip ampache.zip && \
    rm ampache.zip

RUN ln -sf /app/data/rest/htaccess /app/code/rest/.htaccess && \
    ln -sf /app/data/play/htaccess /app/code/play/.htaccess && \
    ln -sf /app/data/channel/htaccess /app/code/channel/.htaccess


RUN mv /app/code/config /app/code/config_bak && ln -sf /app/data/config /app/code/config

RUN chown -R www-data.www-data /app/code

ADD ampache.conf /etc/apache2/sites-enabled/ampache.conf
ADD proftpd.conf /app/code

RUN echo "Listen 8000" > /etc/apache2/ports.conf

ADD start.sh /app/

CMD [ "/app/start.sh" ]
