#!/bin/bash

set -eux

if [[ ! -d /app/data/config ]]; then
    echo "=> Detected first run"

    mkdir -p /app/data/rest /app/data/play /app/data/channel /app/data/music

    # copy config
    cp -r /app/code/config_bak /app/data/config
    cp /app/code/rest/.htaccess.dist /app/data/rest/htaccess
    cp /app/code/play/.htaccess.dist /app/data/play/htaccess
    cp /app/code/channel/.htaccess.dist /app/data/channel/htaccess
    cp /app/data/config/ampache.cfg.php.dist /app/data/config/ampache.cfg.php

    # create database
    mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} < /app/code/sql/ampache.sql 

    # adjust config to fix mobile streaming
    sed -i "s/;http_host = .*/http_host =  \"${APP_DOMAIN}\"/" /app/data/config/ampache.cfg.php
    sed -i "s/;web_path .*/web_path = \"\"/" /app/data/config/ampache.cfg.php
    sed -i "s/;local_web_path .*/local_web_path = \"\"/" /app/data/config/ampache.cfg.php
    sed -i "s/;auth_password_save = .*/auth_password_save = \"true\"/" /app/data/config/ampache.cfg.php

    # ldap stuff in config
    sed -i "s/auth_methods = .*/auth_methods = \"ldap,mysql\"/" /app/data/config/ampache.cfg.php
    sed -i "s/;auto_create = .*/auto_create = \"true\"/" /app/data/config/ampache.cfg.php
    sed -i "s/;auto_user = .*/auto_user = \"user\"/" /app/data/config/ampache.cfg.php
    sed -i "s/;ldap_url = \"ldap:.*/ldap_url = \"ldap:\/\/${LDAP_SERVER}:${LDAP_PORT}\/\"/" /app/data/config/ampache.cfg.php
    sed -i "s/;ldap_search_dn =.*/ldap_search_dn = \"${LDAP_USERS_BASE_DN}\"/" /app/data/config/ampache.cfg.php
    sed -i "s/;ldap_objectclass = \"posix.*/ldap_objectclass = \"user\"/" /app/data/config/ampache.cfg.php
    sed -i "s/;ldap_filter = \"(uid=.*/ldap_filter = \"(username=%v)\"/" /app/data/config/ampache.cfg.php
    sed -i "s/;ldap_name_field = \"display.*/ldap_name_field = \"displayname\"/" /app/data/config/ampache.cfg.php
    sed -i "s/;ldap_email_field =.*/ldap_email_field = \"mail\"/" /app/data/config/ampache.cfg.php

    # update database
    /usr/bin/php /app/code/bin/install/update_db.inc -u

    # create admin user
    /usr/bin/php /app/code/bin/install/add_user.inc -u admin -l 100 -p changeme -e admin@${APP_DOMAIN}
fi

# Prepare SFTP
CODE=/app/code/
DATA=/app/data/
if [ ! -z "$SFTP_PORT" ]; then
    ## render config
    FTP_CFG=proftpd.conf
    [ ! -d "/run/proftpd" ] && mkdir /run/proftpd
    bash ${CODE}${FTP_CFG} > /run/${FTP_CFG}

    ## generate & chmod keys if necessary
    FTPDATA=${DATA}sftpd/
    if [[ ! -f "${FTPDATA}ssh_host_ed25519_key" ]]; then
        echo "Generating ssh host keys"
        mkdir -p ${FTPDATA}
        ssh-keygen -qt rsa -N '' -f ${FTPDATA}ssh_host_rsa_key
        ssh-keygen -qt dsa -N '' -f ${FTPDATA}ssh_host_dsa_key
        ssh-keygen -qt ecdsa -N '' -f ${FTPDATA}ssh_host_ecdsa_key
        ssh-keygen -qt ed25519 -N '' -f ${FTPDATA}ssh_host_ed25519_key
        chmod 0600 ${FTPDATA}*_key
        chmod 0644 ${FTPDATA}*.pub
    fi

    ## daemonize
    /usr/sbin/proftpd -c /run/${FTP_CFG}
fi

chown -R www-data.www-data /app/data /run
chown -R cloudron.cloudron /app/data/music

# update user and pass in case they changed
sed -i "s/database_hostname = .*/database_hostname = \"${MYSQL_HOST}\"/" /app/data/config/ampache.cfg.php
sed -i "s/database_name = .*/database_name = \"${MYSQL_DATABASE}\"/" /app/data/config/ampache.cfg.php
sed -i "s/database_username =.*/database_username = \"${MYSQL_USERNAME}\"/" /app/data/config/ampache.cfg.php
sed -i "s,database_password = .*,database_password = \"${MYSQL_PASSWORD}\"," /app/data/config/ampache.cfg.php


# check if admin user exists, if not, add it
if [ ! $(mysql -N -s --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} -e \
    "select count(*) from user;") -ge 1 ]; then
    # update database
    /usr/bin/php /app/code/bin/install/update_db.inc -u
    # create admin user if not existing
    /usr/bin/php /app/code/bin/install/add_user.inc -u admin -l 100 -p changeme -e admin@${APP_DOMAIN}
else
    echo "all good!"
fi


echo "=> Run ampache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"

exec /usr/sbin/apache2 -DFOREGROUND
