This application integrates with cloudron. However, here's an admin account, all LDAP users are regular users only (which can be changed in the config)

* User: `admin`
* Pass: `changeme`

**Please change the credentials immediately**

Use WinSCP or Cyberduck to connect to this app over SFTP using your cloudron credentials and upload your music collection to /app/data/music
